﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class RenderBSCEffect : MonoBehaviour 
{
	public Shader currentShader;
	public float brightnessAmount = 1.0f;
	public float saturationAmount = 1.0f;
	public float contrastAmount = 1.0f;
	private Material currentMaterial;
	private Camera currCamera;
	
	Material MaterialProp
	{
		get
		{
			if(currentMaterial == null)
			{
				currentMaterial = new Material(currentShader);
				currentMaterial.hideFlags = HideFlags.HideAndDontSave;
			}
			
			return currentMaterial;
		}
	}
	
	void Start () 
	{
		if(!SystemInfo.supportsImageEffects)
		{
			enabled = false;
			return;
		}
		
		if(!currentShader && !currentShader.isSupported)
		{
			enabled = false;
		}
		
		currCamera = GetComponent<Camera> ();
	}
	
	
	void OnRenderImage (RenderTexture sourceTexture, RenderTexture destinationTexture)
	{
		if(currentShader != null)
		{
			MaterialProp.SetFloat("_BrightnessAmount", brightnessAmount);
			MaterialProp.SetFloat("_SaturateAmount", saturationAmount);
			MaterialProp.SetFloat("_ContrastAmount", contrastAmount);

			Graphics.Blit(sourceTexture, destinationTexture, MaterialProp);
		}
		else
		{
			Graphics.Blit(sourceTexture, destinationTexture);
		}
	}
	
	void Update () 
	{
		currCamera.depthTextureMode = DepthTextureMode.Depth;
		brightnessAmount = Mathf.Clamp (brightnessAmount, 0.0f, 2.0f);
		saturationAmount = Mathf.Clamp (saturationAmount, 0.0f, 2.0f);
		contrastAmount = Mathf.Clamp (contrastAmount, 0.0f, 3.0f);
	}
	
	void OnDisable ()
	{
		if(currentMaterial)
		{
			DestroyImmediate(currentMaterial);
		}
	}
}
