﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class RenderBlendModeOverlayEffect : MonoBehaviour 
{
	public Shader currentShader;
	public Texture2D blendTexture;
	public float opacityAmount = 1.0f;
	private Material currentMaterial;

	Material MaterialProp
	{
		get
		{
			if(currentMaterial == null)
			{
				currentMaterial = new Material(currentShader);
				currentMaterial.hideFlags = HideFlags.HideAndDontSave;
			}

			return currentMaterial;
		}
	}
	
	void Start () 
	{
		if(!SystemInfo.supportsImageEffects)
		{
			enabled = false;
			return;
		}

		if(!currentShader && !currentShader.isSupported)
		{
			enabled = false;
		}
	}


	void OnRenderImage (RenderTexture sourceTexture, RenderTexture destinationTexture)
	{
		if(currentShader != null)
		{
			MaterialProp.SetTexture("_BlendTex", blendTexture);
			MaterialProp.SetFloat("_OpacityAmount", opacityAmount);

			Graphics.Blit(sourceTexture, destinationTexture, MaterialProp);
		}
		else
		{
			Graphics.Blit(sourceTexture, destinationTexture);
		}
	}
	
	void Update () 
	{
		opacityAmount = Mathf.Clamp (opacityAmount, 0.0f, 1.0f);
	}

	void OnDisable ()
	{
		if(currentMaterial)
		{
			DestroyImmediate(currentMaterial);
		}
	}
}
