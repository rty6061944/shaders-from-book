﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class RenderDepthImage : MonoBehaviour 
{
	public Shader currentShader;
	public float depthPower = 1.0f;
	private Material currentMaterial;
	private Camera currCamera;

	Material MaterialProp
	{
		get
		{
			if(currentMaterial == null)
			{
				currentMaterial = new Material(currentShader);
				currentMaterial.hideFlags = HideFlags.HideAndDontSave;
			}

			return currentMaterial;
		}
	}
	
	void Start () 
	{
		if(!SystemInfo.supportsImageEffects)
		{
			enabled = false;
			return;
		}

		if(!currentShader && !currentShader.isSupported)
		{
			enabled = false;
		}

		currCamera = GetComponent<Camera> ();
	}


	void OnRenderImage (RenderTexture sourceTexture, RenderTexture destinationTexture)
	{
		if(currentShader != null)
		{
			MaterialProp.SetFloat("_DepthPower", depthPower);
			Graphics.Blit(sourceTexture, destinationTexture, MaterialProp);
		}
		else
		{
			Graphics.Blit(sourceTexture, destinationTexture);
		}
	}
	
	void Update () 
	{
		currCamera.depthTextureMode = DepthTextureMode.Depth;
		depthPower = Mathf.Clamp (depthPower, 0.0f, 5.0f);
	}

	void OnDisable ()
	{
		if(currentMaterial)
		{
			DestroyImmediate(currentMaterial);
		}
	}
}
