﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class RenderGrayScaleImage : MonoBehaviour 
{
	public Shader currentShader;
	public float grayScaleAmount = 1.0f;
	private Material currentMaterial;

	Material MaterialProp
	{
		get
		{
			if(currentMaterial == null)
			{
				currentMaterial = new Material(currentShader);
				currentMaterial.hideFlags = HideFlags.HideAndDontSave;
			}

			return currentMaterial;
		}
	}
	
	void Start () 
	{
		if(!SystemInfo.supportsImageEffects)
		{
			enabled = false;
			return;
		}

		if(!currentShader && !currentShader.isSupported)
		{
			enabled = false;
		}
	}


	void OnRenderImage (RenderTexture sourceTexture, RenderTexture destinationTexture)
	{
		if(currentShader != null)
		{
			MaterialProp.SetFloat("_LuminosityAmount", grayScaleAmount);
			Graphics.Blit(sourceTexture, destinationTexture, MaterialProp);
		}
		else
		{
			Graphics.Blit(sourceTexture, destinationTexture);
		}
	}
	
	void Update () 
	{
		grayScaleAmount = Mathf.Clamp (grayScaleAmount, 0.0f, 1.0f);
	}

	void OnDisable ()
	{
		if(currentMaterial)
		{
			DestroyImmediate(currentMaterial);
		}
	}
}
