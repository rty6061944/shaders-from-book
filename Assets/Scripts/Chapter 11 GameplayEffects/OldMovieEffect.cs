﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class OldMovieEffect : MonoBehaviour 
{
	public Shader oldFilmShader;
	public float oldFilmEffectAmount = 1.0f;
	public Color SepiaColor = Color.white;

	public Texture2D vignetteTexture;
	public float vignetteAmount = 1.0f;

	public Texture2D dustTexture;
	public float dustSpeedX = 10.0f;
	public float dustSpeedY = 10.0f;

	public Texture2D scratchTexture;
	public float scratchSpeedX = 10.0f;
	public float scratchSpeedY = 10.0f;

	private Material currentMaterial;
	private float randomFloat;

	Material MaterialProp
	{
		get
		{
			if(currentMaterial == null)
			{
				currentMaterial = new Material(oldFilmShader);
				currentMaterial.hideFlags = HideFlags.HideAndDontSave;
			}

			return currentMaterial;
		}
	}
	
	void Start () 
	{
		if(!SystemInfo.supportsImageEffects)
		{
			enabled = false;
			return;
		}

		if(!oldFilmShader && !oldFilmShader.isSupported)
		{
			enabled = false;
		}
	}


	void OnRenderImage (RenderTexture sourceTexture, RenderTexture destinationTexture)
	{
		if(oldFilmShader != null)
		{
			MaterialProp.SetColor("_SepiaColor", SepiaColor);
			MaterialProp.SetFloat("_VignetteAmount", vignetteAmount);
			MaterialProp.SetFloat("_OldFilmEffectAmount", oldFilmEffectAmount);

			if(vignetteTexture)
			{
				MaterialProp.SetTexture("_VignetteTexture", vignetteTexture);
			}

			if(scratchTexture)
			{
				MaterialProp.SetTexture("_ScratchTexture", scratchTexture);
				MaterialProp.SetFloat("_ScratchSpeedX", scratchSpeedX);
				MaterialProp.SetFloat("_ScratchSpeedY", scratchSpeedY);
			}

			if(dustTexture)
			{
				MaterialProp.SetTexture("_DustTexture", dustTexture);
				MaterialProp.SetFloat("_DustSpeedX", dustSpeedX);
				MaterialProp.SetFloat("_DustSpeedY", dustSpeedY);
				MaterialProp.SetFloat("_RandomFloat", randomFloat);
			}

			Graphics.Blit(sourceTexture, destinationTexture, MaterialProp);
		}
		else
		{
			Graphics.Blit(sourceTexture, destinationTexture);
		}
	}
	
	void Update () 
	{
		vignetteAmount = Mathf.Clamp01 (vignetteAmount);
		oldFilmEffectAmount = Mathf.Clamp (oldFilmEffectAmount, 0.0f, 1.5f);
		randomFloat = Random.Range (-1f, 1f);
	}

	void OnDisable ()
	{
		if(currentMaterial)
		{
			DestroyImmediate(currentMaterial);
		}
	}
}
