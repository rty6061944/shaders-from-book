﻿using UnityEngine;
using System.Collections;

public class ProceduralTexture : MonoBehaviour 
{
	[SerializeField]
	private int textureWidth;

	[SerializeField]
	private Texture2D generatedTexture;

	private Material currentMaterial;
	private Vector2 centerPosition;

	void Awake ()
	{
		if(!currentMaterial)
		{
			currentMaterial = GetComponent<Renderer>().sharedMaterial;

			if(!currentMaterial)
				Debug.Log ("There is no material at all");
		}

		if(currentMaterial)
		{
			centerPosition = new Vector2(0.5f, 0.5f);
			generatedTexture = GenerateGradient ();
			currentMaterial.SetTexture("_MainTexture", generatedTexture);
		}
	}

	Texture2D GenerateGradient ()
	{
		Texture2D proceduralTexture = new Texture2D (textureWidth, textureWidth);
		Vector2 centerPixelPosition = centerPosition * textureWidth;

		for(int heightIndex = 0; heightIndex < textureWidth; heightIndex ++)
		{
			for(int widthIndex = 0; widthIndex < textureWidth; widthIndex ++)
			{
				Vector2 currentPosition = new Vector2(heightIndex, widthIndex);
				float pixelDistance = Vector2.Distance(currentPosition, centerPixelPosition) / textureWidth * 0.5f;

				pixelDistance = Mathf.Abs(1 - Mathf.Clamp(pixelDistance, 0f, 1f));
				Color pixelColor = new Color(pixelDistance, pixelDistance, pixelDistance, 1);

				proceduralTexture.SetPixel(heightIndex, widthIndex, pixelColor);
			}
		}

		proceduralTexture.Apply ();

		return proceduralTexture;
	}
}
