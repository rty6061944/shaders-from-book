﻿using UnityEngine;
using System.Collections;

public class ProceduralTexture3 : MonoBehaviour 
{
	[SerializeField]
	private int textureWidth;

	[SerializeField]
	private Texture2D generatedTexture;

	private Material currentMaterial;
	private Vector2 centerPosition;

	void Awake ()
	{
		if(!currentMaterial)
		{
			currentMaterial = GetComponent<Renderer>().sharedMaterial;

			if(!currentMaterial)
				Debug.Log ("There is no material at all");
		}

		if(currentMaterial)
		{
			centerPosition = new Vector2(0.5f, 0.5f);
			generatedTexture = GenerateGradient ();
			currentMaterial.SetTexture("_MainTexture", generatedTexture);
		}
	}

	Texture2D GenerateGradient ()
	{
		Texture2D proceduralTexture = new Texture2D (textureWidth, textureWidth);
		Vector2 centerPixelPosition = centerPosition * textureWidth;

		for(int heightIndex = 0; heightIndex < textureWidth; heightIndex ++)
		{
			for(int widthIndex = 0; widthIndex < textureWidth; widthIndex ++)
			{
				Vector2 currentPosition = new Vector2(heightIndex, widthIndex);

				Vector2 pixelDirection = centerPixelPosition - currentPosition;
				pixelDirection.Normalize ();
				float rightDirection = Vector2.Dot(pixelDirection, Vector2.right);
				float leftDirection = Vector2.Dot(pixelDirection, -Vector2.right);
				float upDirection = Vector2.Dot(pixelDirection, Vector2.up);


				Color pixelColor = new Color(rightDirection, leftDirection, upDirection, 1);
				proceduralTexture.SetPixel(heightIndex, widthIndex, pixelColor);
			}
		}

		proceduralTexture.Apply ();

		return proceduralTexture;
	}
}
