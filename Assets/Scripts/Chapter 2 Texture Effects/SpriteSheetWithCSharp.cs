﻿using UnityEngine;

public class SpriteSheetWithCSharp : MonoBehaviour 
{
	public float speed;

	float frame;
	
	void Update () 
	{
		frame = Mathf.Ceil (Time.time * speed % 8);
        GetComponent<Renderer>().material.SetFloat("_Frame", frame);
	}
}