﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class GenerateStaticCubemap : ScriptableWizard 
{
	[MenuItem("Cookbook/Render cubemap")]
	static void renderCubemap ()
	{
		ScriptableWizard.DisplayWizard("Render cubemap", typeof(GenerateStaticCubemap), "Render");
	}

	public Transform renderPosition;
	public Cubemap cubeMap;

	void OnWizardUpdate ()
	{
		helpString = "Select transform to render and form a cubemap for it";

		if(renderPosition != null && cubeMap != null)
			isValid = true;
		else
			isValid = false;
	}

	void OnWizardCreate ()
	{
		GameObject go = new GameObject ("CubeCam", typeof(Camera));

		go.transform.position = renderPosition.position;
		go.transform.rotation = Quaternion.identity;

        go.GetComponent<Camera>().RenderToCubemap(cubeMap);

		DestroyImmediate (go);
	}
}