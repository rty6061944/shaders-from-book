﻿using UnityEngine;
using System.Collections;

public class MouseController : MonoBehaviour 
{
	public Transform posA;
	public Transform posB;
	
	void Update () 
	{
		if(Input.GetMouseButton(0))
		{
			this.gameObject.transform.position = Vector3.MoveTowards(this.gameObject.transform.position, posA.position, Time.deltaTime * 1);
		}

		if(Input.GetMouseButton(1))
		{
			this.gameObject.transform.position = Vector3.MoveTowards(this.gameObject.transform.position, posB.position, Time.deltaTime * 1);
		}
	}
}
