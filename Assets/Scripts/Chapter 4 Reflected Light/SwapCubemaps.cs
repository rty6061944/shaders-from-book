﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class SwapCubemaps : MonoBehaviour 
{
	public Cubemap cubeA;
	public Cubemap cubeB;

	public Transform posA;
	public Transform posB;

	private Material material;
	private Cubemap currentCubemap;
	
	// Update is called once per frame
	void Update () 
	{
		material = GetComponent<MeshRenderer> ().sharedMaterial;
	//	material = GetComponent<MeshRenderer> ().materials[0];

		if(material)
		{
			currentCubemap = CheckProbeDistance();
			material.SetTexture("_Cubemap", currentCubemap);
		}
	}

	void OnDrawGizmos ()
	{
		Gizmos.color = Color.green;

		if(posA)
		{
			Gizmos.DrawWireSphere(posA.position, 0.5f);
		}
		if(posB)
		{
			Gizmos.DrawWireSphere(posB.position, 0.5f);
		}
	}

	Cubemap CheckProbeDistance ()
	{
		float distanceA = Vector3.Distance (transform.position, posA.position);
		float distanceB = Vector3.Distance (transform.position, posB.position);

		if(distanceA > distanceB)
		{
			return cubeB;
		}
		else
		{
			return cubeA;
		}
	}
}
