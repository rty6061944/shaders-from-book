﻿using UnityEngine;
using UnityEngine.UI;

public class UIWindow : MonoBehaviour 
{
	[SerializeField]
	private Texture2D windowTexture;
	private Texture2D faderTexture;
	
	private Image imageComponent;
	private int textureWidth = 512;
	private Vector2 centerPosition;
	
	void Awake ()
	{
		centerPosition = new Vector2(0.5f, 0.5f);
		imageComponent = GetComponent<Image> ();
	//	faderTexture = imageComponent.sprite;


		imageComponent.sprite = GenerateTexture ();
	}

	Sprite GenerateTexture ()
	{
		Texture2D proceduralTexture = new Texture2D (textureWidth, textureWidth);
		Vector2 centerPixelPosition = centerPosition * textureWidth;
		
		for(int heightIndex = 0; heightIndex < textureWidth; heightIndex ++)
		{
			for(int widthIndex = 0; widthIndex < textureWidth; widthIndex ++)
			{
				Vector2 currentPosition = new Vector2(heightIndex, widthIndex);
				float pixelDistance = Vector2.Distance(currentPosition, centerPixelPosition) / textureWidth * 0.5f;
				
		//		Debug.Log ("Alpha  is  "  + Mathf.Abs( 1 - windowTexture.GetPixel(heightIndex, widthIndex).a));
				Color pixelColor = new Color(0, 0, 0, Mathf.Abs( 1 - windowTexture.GetPixel(heightIndex, widthIndex).a));

				proceduralTexture.SetPixel(heightIndex, widthIndex, pixelColor);
			}
		}

		proceduralTexture.Apply ();

		Rect rect = new Rect(0, 0, proceduralTexture.width, proceduralTexture.height);
		Sprite neededSprite =  Sprite.Create(proceduralTexture, rect, centerPosition);
		
		return neededSprite;
	}
}
