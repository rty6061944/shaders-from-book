﻿using UnityEngine;
using System.Collections;

public class MeshColors : MonoBehaviour 
{
	void Start() 
	{
		Mesh mesh = GetComponent<MeshFilter>().mesh;
		Vector3[] vertices = mesh.vertices;
		Color[] colors = new Color[vertices.Length];
		int i = 0;

		while (i < vertices.Length) 
		{
			if(i < vertices.Length)
			{
				float t = (1.0f * i) / vertices.Length;

				colors[i] = Color.Lerp(Color.white, Color.black, t);
			}

			i++;
		}

		mesh.colors = colors;
	}
}	
