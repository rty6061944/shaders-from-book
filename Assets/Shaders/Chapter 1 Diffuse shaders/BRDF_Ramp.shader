﻿Shader "Custom/Cookbook Shaders/Chapter 1/BRDF_Ramp" 
{
	Properties 
	{
		_EmmisiveColor ("Emmisive Color", Color) = (1, 1, 1, 1)
		_AmbientColor ("Ambient Color", Color) = (1, 1, 1, 1)
		_MySliderValue ("This is a slider", Range(0, 10)) = 8
		_RampTex ("Base (RGB)", 2D) = "white" {}
	}
	
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM		
		#pragma surface surf CustomDiffuse
		
		float4 _EmmisiveColor;
		float4 _AmbientColor;
		float _MySliderValue;
		sampler2D _RampTex;
	
		struct Input 
		{
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			float4 c;
			c = pow((_EmmisiveColor + _AmbientColor), _MySliderValue);
			
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		
		inline float4 LightingCustomDiffuse (SurfaceOutput s, fixed3 lightDir, float3 viewDir, fixed atten)
		{
			float difLight = max (0, dot(s.Normal, lightDir));
			float rimLight = dot(s.Normal, viewDir);
			float halfLambert = difLight * 0.5 + 0.5;
			float2 halfLambertUV = float2(halfLambert, rimLight);
			float3 rampTexture = tex2D (_RampTex, halfLambertUV).rgb; 
			
			float4 myColor;
			myColor.rgb = s.Albedo * _LightColor0.rgb * rampTexture;
			myColor.a = s.Alpha;
			return myColor;
		}

		ENDCG
	} 
	FallBack "Diffuse"
}
