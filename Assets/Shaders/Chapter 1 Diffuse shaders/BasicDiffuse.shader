﻿Shader "Custom/Cookbook Shaders/Chapter 1/BasicDiffuse" 
{
	Properties 
	{
		_EmmisiveColor ("Emmisive Color", Color) = (1, 1, 1, 1)
		_AmbientColor ("Ambient Color", Color) = (1, 1, 1, 1)
		_MySliderValue ("This is a slider", Range(0, 10)) = 8
	}
	
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM		
		#pragma surface surf Lambert
		
		float4 _EmmisiveColor;
		float4 _AmbientColor;
		float _MySliderValue;

		struct Input 
		{
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			float4 c;
			c = pow((_EmmisiveColor + _AmbientColor), _MySliderValue);
			
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
