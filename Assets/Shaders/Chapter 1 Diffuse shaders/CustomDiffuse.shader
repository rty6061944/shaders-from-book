﻿Shader "Custom/Cookbook Shaders/Chapter 1/CustomDiffuse" 
{
	Properties 
	{
		_EmmisiveColor ("Emmisive Color", Color) = (1, 1, 1, 1)
		_AmbientColor ("Ambient Color", Color) = (1, 1, 1, 1)
		_MySliderValue ("This is a slider", Range(0, 10)) = 8
	}
	
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM		
		#pragma surface surf CustomDiffuse
		
		float4 _EmmisiveColor;
		float4 _AmbientColor;
		float _MySliderValue;
	
		struct Input 
		{
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			float4 c;
			c = pow((_EmmisiveColor + _AmbientColor), _MySliderValue);
			
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		
		inline float4 LightingCustomDiffuse (SurfaceOutput surface, fixed3 lightDir, fixed atten)
		{
			float difLight = max(0, dot(surface.Normal, lightDir));
			float4 myColor;
			myColor.rgb = surface.Albedo * _LightColor0.rgb * (difLight * atten * 2);
			myColor.a = surface.Alpha;
			return myColor;
		}

		ENDCG
	} 
	FallBack "Diffuse"
}
