﻿Shader "Custom/Cookbook Shaders/Chapter 1/Possible_Properties" {
	Properties 
	{
		_Slider  ("Slider Property", Range(0, 10)) = 9
		_Color   ("Color Property", Color) = (1, 1, 1, 1)
		_Texture ("2D Texture Property", 2D) = "Texture" {}
		_Rect    ("Rectangle Property NON Power of 2", Rect) = "Rect" {}
		_Cube    ("Cubemap texture Property", Cube) = "Cube" {}
		_Float   ("Float Property", float) = 115.5
		_Vector  ("Vector Property", Vector) = (15, 10, 3, 2)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
