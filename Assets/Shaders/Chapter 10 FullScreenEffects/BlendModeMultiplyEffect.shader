﻿Shader "Custom/Cookbook Shaders/Chapter 10/BlendModeMultiplyEffect" 
{
	Properties 
	{
		_MainTex ("Main Texture", 2D) = "white" {}
		_BlendTex ("Blend Texture", 2D) = "white" {}
		_OpacityAmount ("Blend Opacity", Range(0, 1)) = 1.0
	}
	
	SubShader 
	{
		Pass
		{
		//	Blend SrcColor SrcColor
		
			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "UnityCG.cginc"
			
			uniform sampler2D _MainTex;
			uniform sampler2D _BlendTex;
			fixed _OpacityAmount;
			
			fixed4 frag (v2f_img image) : COLOR
			{
				fixed4 renderTex = tex2D (_MainTex, image.uv);
				fixed4 blendTex = tex2D (_BlendTex, image.uv);
				fixed4 blendMultiply = renderTex * blendTex;
				
				renderTex = lerp (renderTex, blendMultiply, _OpacityAmount);
				
				return renderTex;
			}
			
			ENDCG
		}
	}
	
	FallBack "Diffuse"
}
