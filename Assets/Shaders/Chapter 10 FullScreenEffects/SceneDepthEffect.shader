﻿Shader "Custom/Cookbook Shaders/Chapter 10/GrayImageEffect" 
{
	Properties 
	{
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_DepthPower ("_Depth Power", Range(0, 5)) = 1.0
	}
	
	SubShader 
	{
		Pass
		{
			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "UnityCG.cginc"
			
			uniform sampler2D _MainTex;
			sampler2D _CameraDepthTexture;
			fixed _DepthPower;
			
			fixed4 frag (v2f_img image) : COLOR
			{
				float d = UNITY_SAMPLE_DEPTH (tex2D (_CameraDepthTexture, image.uv.xy));
				d = pow (Linear01Depth(d), _DepthPower);
				
				return d;
			}
			
			ENDCG
		}
	}
	
	FallBack "Diffuse"
}
