﻿Shader "Custom/Cookbook Shaders/Chapter 11/OldMovieEffect" 
{
	Properties 
	{
		_MainTex ("Main Texture", 2D) = "white" {}
		_VignetteTexture ("Vignette Texture", 2D) = "white" {}
		_ScratchTexture ("Scratch Texture", 2D) = "white" {}
		_DustTexture ("Dust Texture", 2D) = "white" {}
		
		
		_SepiaColor ("Sepia Color", Color) = (1, 1, 1, 1)
		_OldFilmEffectAmount ("Old Film Effect Amount", Range(0, 1)) = 1.0
		_VignetteAmount ("_Vignette Amount", Range(0, 1)) = 1.0
		
		_ScratchSpeedX ("_Scratch Speed X", Float) = 10.0
		_ScratchSpeedY ("_Scratch Speed Y", Float) = 10.0
		_DustSpeedX ("_Dust Speed X", Float) = 10.0
		_DustSpeedY ("_Dust Speed Y", Float) = 10.0
		_RandomFloat ("Random Float", Float) = 10.0
	}
	
	SubShader 
	{
		Pass
		{
			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "UnityCG.cginc"
			
			uniform sampler2D _MainTex;
			uniform sampler2D _VignetteTexture;
			uniform sampler2D _ScratchTexture;
			uniform sampler2D _DustTexture;
			
			fixed4 _SepiaColor;
			
			fixed _OldFilmEffectAmount;
			fixed _VignetteAmount;
			fixed _ScratchSpeedX;
			fixed _ScratchSpeedY;
			fixed _DustSpeedX;
			fixed _DustSpeedY;
			fixed _RandomFloat;
			
			
			fixed4 frag (v2f_img image) : COLOR
			{
				half2 renderTexUV = half2(image.uv.x, image.uv.y + (_RandomFloat * _SinTime.z * 0.005));
				fixed4 renderTex = tex2D (_MainTex, renderTexUV);
			
				fixed4 vignetteTex = tex2D (_VignetteTexture, image.uv);
				
				half2 scratchesUV = half2 (image.uv.x + (_RandomFloat * _SinTime.z * _ScratchSpeedX), image.uv.y + (_Time.x * _ScratchSpeedY));
				fixed4 scratchesTex = tex2D (_ScratchTexture, scratchesUV);
				
				half2 dustUV = half2 (image.uv.x + (_RandomFloat * _SinTime.z * _DustSpeedX), image.uv.y + (_RandomFloat * _SinTime.z * _DustSpeedY));
				fixed4 dustTex = tex2D (_DustTexture, dustUV);
				
				fixed lum = dot (fixed3 (0.299, 0.587, 0.114), renderTex.rgb);
				fixed4 finalColor = lum + lerp (_SepiaColor, _SepiaColor + fixed4(0.1f, 0.1f, 0.1f, 0.1f), _RandomFloat);
				
				fixed3 constantWhite = fixed3(1, 1, 1);
				finalColor = lerp (finalColor, finalColor * vignetteTex, _VignetteAmount);
				finalColor.rgb *= lerp (scratchesTex, constantWhite, _RandomFloat); 
				finalColor.rgb *= lerp (dustTex.rgb, constantWhite, (_RandomFloat * _SinTime.z));
				finalColor = lerp (renderTex, finalColor, _OldFilmEffectAmount);

				return finalColor;
			}
			
			ENDCG
		}
	}
	
	FallBack "Diffuse"
}
