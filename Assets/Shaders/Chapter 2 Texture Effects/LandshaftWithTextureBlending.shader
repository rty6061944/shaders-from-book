﻿Shader "Custom/CookbookShader/Chapter 2/LandshaftWithTextureBlending" 
{
	Properties 
	{
		_Tint ("DiffuseTint", Color) = (1, 1, 1, 1)
		_ColorA ("Terrain Color A", Color) = (1, 1, 1, 1)
		_ColorB ("Terrain Color B", Color) = (1, 1, 1, 1)
	
		_RChannelTex ("RChannelTex", 2D) = "" {}
		_GChannelTex ("GChannelTex", 2D) = "" {}
		_BChannelTex ("BChannelTex", 2D) = "" {}
		_AChannelTex ("AChannelTex", 2D) = "" {}
		_BlendTex ("BlendTex", 2D) = "" {}
	}
	
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert
		#pragma target 3.0
		#pragma debug
		
		float4 _Tint;
		float4 _ColorA;
		float4 _ColorB;
		
		sampler2D _RChannelTex;
		sampler2D _GChannelTex;
		sampler2D _BChannelTex;
		sampler2D _AChannelTex;
		sampler2D _BlendTex;

		struct Input 
		{
			float2 uv_BlendTex;
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			float4 BlendData = tex2D (_BlendTex, IN.uv_BlendTex);
			float4 RChannelData = tex2D (_RChannelTex, IN.uv_BlendTex);
			float4 GChannelData = tex2D (_GChannelTex, IN.uv_BlendTex);
			float4 BChannelData = tex2D (_BChannelTex, IN.uv_BlendTex);
			float4 AChannelData = tex2D (_AChannelTex, IN.uv_BlendTex);
			
			float4 finalColor;
			finalColor = lerp (RChannelData, GChannelData, BlendData.g);
			finalColor = lerp (finalColor, BChannelData, BlendData.b);
			finalColor = lerp (finalColor, AChannelData, BlendData.r);
			finalColor.a = 1.0;
		
			float4 terrainColor = lerp (_ColorA, _ColorB, BlendData.a);
			finalColor *= terrainColor;
			finalColor = saturate(finalColor);
		
			o.Albedo = finalColor.rgb * _Tint.rgb;
		//	o.Albedo = _Tint.rgb;
			o.Alpha = finalColor.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
