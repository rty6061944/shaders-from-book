﻿Shader "Custom/Cookbook Shaders/Chapter 2/NormalMap" 
{
	Properties 
	{
		_DiffuseTint ("DiffuseTint", Color) = (1, 1, 1, 1)
		_NormalMapTex ("NormalMapTex", 2D) = "" {}
	}

	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		float4 _DiffuseTint;
		sampler2D _NormalMapTex;

		struct Input 
		{
			float2 uv_NormalMapTex;
		};

		void surf (Input IN, inout SurfaceOutput output) 
		{
			float3 normalMap = UnpackNormal(tex2D(_NormalMapTex, IN.uv_NormalMapTex));
			
			output.Normal = normalMap.rgb;
			output.Albedo = _DiffuseTint.rgb;
			output.Alpha = _DiffuseTint.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
