﻿Shader "Custom/Cookbook Shaders/Chapter 2/NormalMapWithSettings" 
{
	Properties 
	{
		_DiffuseTint ("DiffuseTint", Color) = (1, 1, 1, 1)
		_NormalMapTex ("NormalMapTex", 2D) = "" {}
		_NormalIntensity ("Normal Map Intensity", Range (0, 2)) = 1
	}
	
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		float _NormalIntensity;
		float4 _DiffuseTint;
		sampler2D _NormalMapTex;

		struct Input 
		{
			float2 uv_NormalMapTex;
		};

		void surf (Input IN, inout SurfaceOutput output) 
		{
			float3 normalMap = UnpackNormal(tex2D(_NormalMapTex, IN.uv_NormalMapTex));
			
			float normalMapX = normalMap.x * _NormalIntensity;
			float normalMapY = normalMap.y * _NormalIntensity;
			
			normalMap = float3(normalMapX, normalMapY, normalMap.z);
			
			output.Normal = normalMap.rgb;
			output.Albedo = _DiffuseTint.rgb;
			output.Alpha = _DiffuseTint.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
