﻿Shader "Custom/Cookbook Shaders/Chapter 2/ProceduralTexture" 
{
	Properties 
	{
		_MainTexture ("MainTexture", 2D) = "white" {}
	}
	
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D _MainTexture;

		struct Input 
		{
			float2 uv_MainTexture;
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			half4 c = tex2D (_MainTexture, IN.uv_MainTexture);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
