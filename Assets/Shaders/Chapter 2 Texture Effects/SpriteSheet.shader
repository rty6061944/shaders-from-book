﻿Shader "Custom/CookbookShader/Chapter 2/SpriteSheet" 
{
	Properties 
	{
		_MainTex ("MainTexture", 2D) = "white" {}
		_CellAmount ("CellAmmount", float) = 0.0
		_Speed ("Speed", Range (0.01, 32)) = 8
	}
	
	SubShader 
	{
		Tags { "Queue"="Transparent" "RenderType"="Transparent" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert alpha

		float _CellAmount;
		float _Speed;
		sampler2D _MainTex;

		struct Input 
		{
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			float2 spriteUV = IN.uv_MainTex;
			float cellUVPercentage = 1/_CellAmount;
			float frame = fmod(_Time.y * _Speed, _CellAmount);
			frame = floor (frame);
			
			float xValue = (spriteUV.x + frame) * cellUVPercentage;
			spriteUV = float2 (xValue, spriteUV.y);
		
			half4 c = tex2D (_MainTex, spriteUV);
			
			o.Albedo = c.rgb;
			o.Alpha = c.a;
			
			if(c.a < 0.1f)
			{
				o.Albedo = float3 (0, 0, 0);
			}
		}
		
		ENDCG
	} 
	FallBack "Diffuse"
}
