﻿Shader "Custom/Cookbook Shaders/Chapter 2/SpriteSheetWithC#" 
{
	Properties 
	{
		_Frame ("Frame", float) = 0.0
		_MainTex ("MainTexture", 2D) = "white" {}
		_CellAmount ("CellAmmount", float) = 0.0
		_Speed ("Speed", Range (0.01, 32)) = 8
	}
	
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		float _CellAmount;
		float _Speed;
		float _Frame;
		sampler2D _MainTex;
		
		struct Input 
		{
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			float2 spriteUV = IN.uv_MainTex;
			float cellUVPercentage = 1/_CellAmount;
			
			float xValue = (spriteUV.x + _Frame) * cellUVPercentage;
			spriteUV = float2 (xValue, spriteUV.y);
		
			half4 c = tex2D (_MainTex, spriteUV);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		
		ENDCG
	} 
	FallBack "Diffuse"
}
