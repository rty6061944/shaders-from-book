﻿Shader "Custom/CookbookShader/Chapter 2/UVScroll" 
{
	Properties 
	{
		_MainTexture ("MainTexture", 2D) = "white" {}
		_MainTint ("MainTint", Color) = (1, 1, 1, 1)
		_ScrollXSpeed ("X Scrolling Speed", Range(0, 10)) = 0
		_ScrollYSpeed ("Y Scrolling Speed", Range(0, 10)) = 2
	}
	
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		fixed4 _MainTint;
		fixed _ScrollXSpeed;
		fixed _ScrollYSpeed;
		sampler2D _MainTexture;

		struct Input 
		{
			float2 uv_MainTexture;
			float4 _MainTex_ST; 
		};

		void surf (Input IN, inout SurfaceOutput output) 
		{
			fixed2 scrolledUV = IN.uv_MainTexture;
			
			fixed xScrollValue = _ScrollXSpeed * _Time.y;
			fixed yScrollValue = _ScrollYSpeed * _Time.y;
			
			scrolledUV += fixed2 (xScrollValue, yScrollValue);
			
			if(scrolledUV.y > 1)
			{
				float offs = frac (scrolledUV.y - 1);
				scrolledUV.y = offs;
			}

			half4 c = tex2D (_MainTexture, scrolledUV);
			output.Albedo = c.rgb * _MainTint;
			output.Alpha = c.a;
		}
		ENDCG
	} 
	
	FallBack "Diffuse"
}
