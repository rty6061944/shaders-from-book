﻿Shader "Custom/Cookbook Shaders/Chapter 3/BlinnPhong" 
{
	Properties 
	{
		_MainTexture ("MainTexture", 2D) = "white" {}
		_MainTint ("MainTint", Color) = (1, 1, 1, 1)
		_SpecularColor ("SpecularColor", Color) = (1, 1, 1, 1)
		_SpecularPower ("SpecularPower", Range(0.1, 60)) = 3
		
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf CustomBlinnPhong

		sampler2D _MainTexture;
		float4 _MainTint;	
		float4 _SpecularColor;
		float _SpecularPower;
		
		struct Input 
		{
			float2 uv_MainTexture;
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			half4 c = tex2D (_MainTexture, IN.uv_MainTexture) * _MainTint;
			
			o.Specular = _SpecularPower;
			o.Gloss = 1.0;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		
		inline fixed4 LightingCustomBlinnPhong (SurfaceOutput output, fixed3 lightDir, half3 viewDir, fixed atten)
		{
			float3 halfVector = normalize (lightDir + viewDir);
			float diff =  max(0, dot (output.Normal, lightDir));
			
			float nh = max (0, dot (output.Normal, halfVector));
			float spec = pow (nh, _SpecularPower) * _SpecularColor;

			fixed4 c;
			c.rgb = (output.Albedo * _LightColor0.rgb * diff) + (_LightColor0.rgb * _SpecularColor.rgb * spec) * (atten * 2);
			c.a = output.Alpha;
			
			return c;
		}
		
		ENDCG
	} 
	FallBack "Diffuse"
}
