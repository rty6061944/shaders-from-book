﻿Shader "Custom/Cookbook Shaders/Chapter 3/DiffSpecular" 
{
	Properties 
	{
		_MainTexture ("MainTexture ", 2D) = "white" {}
		_RoughnessTexture ("RoughnessTexture", 2D) = "white" {}
		
		_MainTint ("MainTint", Color) = (1, 1, 1, 1)
		_SpecularColor ("SpecularColor", Color) = (1, 1, 1, 1)
		
		_Fresnel ("Fresnel", Range(0, 1)) = 0.05
		_Roughness ("Roughness", Range(0, 1)) = 0.5
		_SpecularPower ("SpecularPower", Range(0, 30)) = 2
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf MetallicSoft
		#pragma target 3.0

		sampler2D _MainTexture;
		sampler2D _RoughnessTexture;
		
		float4 _MainTint;	
		float4 _SpecularColor;
		
		float _Fresnel;
		float _Roughness;
		float _SpecularPower;
		
		struct Input 
		{
			float2 uv_MainTexture;
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			half4 c = tex2D (_MainTexture, IN.uv_MainTexture) * _MainTint;
			
			o.Specular = _SpecularPower;
			o.Gloss = 1.0;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		
		inline fixed4 LightingMetallicSoft (SurfaceOutput output, fixed3 lightDir, half3 viewDir, fixed atten)
		{
			// диффузия и направление взгляда
			float3 halfVector = normalize (lightDir + viewDir);
			float NdotL = saturate (dot (output.Normal, normalize(lightDir)));
			float NdotH_raw = dot (output.Normal, halfVector);
			float NdotH = saturate (NdotH_raw);
			float NdotV = saturate (dot (output.Normal, normalize(viewDir)));
			float VdotH = saturate (dot (halfVector, normalize(viewDir)));
			
			// распределение неровностей
			float geoEnum = 2.0 * NdotH;
			float3 G1 = (geoEnum * NdotV) / NdotH;
			float3 G2 = (geoEnum * NdotL) / NdotH;
			float3 G = min (1.0, min (G1, G2));
			
			// достанем цвет из BRDF текстуры
			float roughness = tex2D (_RoughnessTexture, float2(NdotH_raw * 0.5 + 0.5, _Roughness)).r;
			
			// вычисление эффекта Френеля: маскируем блик, если угол просмотра слишком острый
			float fresnel = pow (1.0 - VdotH, 5.0);
			fresnel *= (1.0 - _Fresnel);
			fresnel += _Fresnel;
			
			float3 spec = float3(fresnel * G * roughness * roughness) * _SpecularPower;
			
			fixed4 c;
			c.rgb = (output.Albedo * _LightColor0.rgb * NdotL) + (spec * _SpecularColor.rgb) * (atten * 2);
			c.a = output.Alpha;
			
			return c;
		}
		
		ENDCG
	} 
	FallBack "Diffuse"
}
