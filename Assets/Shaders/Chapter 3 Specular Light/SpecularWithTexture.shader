﻿Shader "Custom/Cookbook Shaders/Chapter 3/SpecularWithTexture" 
{
	Properties 
	{
		_MainTexture ("MainTexture", 2D) = "white" {}
		_SpecularMask ("SpecularMask", 2D) = "white" {}
		_MainTint ("MainTint", Color) = (1, 1, 1, 1)
		_SpecularColor ("SpecularColor", Color) = (1, 1, 1, 1)
		_SpecularPower ("SpecularPower", Range(0.1, 120)) = 3
		
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf CustomPhong

		sampler2D _MainTexture;
		sampler2D _SpecularMask;
		float4 _MainTint;
		float _SpecularPower;
		float4 _SpecularColor;

		struct SurfaceCustomOutput
		{
			fixed3 Albedo;
			fixed3 Normal;
			fixed3 Emission;
			fixed3 SpecularColor;
			half Specular;
			fixed Gloss;
			fixed Alpha;
		};

		struct Input 
		{
			float2 uv_MainTexture;
			float2 uv_SpecularMask;
		};

		void surf (Input IN, inout SurfaceCustomOutput o) 
		{
			float4 c = tex2D (_MainTexture, IN.uv_MainTexture) * _MainTint;
			float4 SpecMask = tex2D (_SpecularMask, IN.uv_SpecularMask) * _SpecularColor;
			
			o.Specular = SpecMask.r;
			o.SpecularColor = SpecMask.rgb;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		
		inline fixed4 LightingCustomPhong (SurfaceCustomOutput output, fixed3 lightDir, half3 viewDir, fixed atten)
		{
			float diff = dot (output.Normal, lightDir);
			float3 reflectionVector = normalize (2.0 * output.Normal * diff - lightDir);
			
			float spec = pow (max(0, dot (reflectionVector, viewDir)), _SpecularPower) * output.Specular;
			float finalSpec = output.SpecularColor * _SpecularColor.rgb * spec;
			
			fixed4 c;
			c.rgb = (output.Albedo * _LightColor0.rgb * diff) + (_LightColor0.rgb * finalSpec);
			c.a = output.Alpha;
			
			return c;
		}
		
		ENDCG
	} 
	FallBack "Diffuse"
}
