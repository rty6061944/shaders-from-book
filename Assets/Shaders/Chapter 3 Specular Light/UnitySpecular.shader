﻿Shader "Custom/Cookbook Shaders/Chapter 3/UnitySpecular" 
{
	Properties 
	{
		_MainTexture ("MainTexture", 2D) = "white" {}
		_MainTint ("MainTint", Color) = (1, 1, 1, 1)
		_SpecColor ("SpecularColor", Color) = (1, 1, 1, 1)
		_SpecularPower ("SpecularPower", Range(0, 1)) = 0.5
		
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf BlinnPhong

		sampler2D _MainTexture;
		float4 _MainTint;
		float _SpecularPower;

		struct Input 
		{
			float2 uv_MainTexture;
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			half4 c = tex2D (_MainTexture, IN.uv_MainTexture) * _MainTint;
			
			o.Specular = _SpecularPower;
			o.Gloss = 1.0;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
