﻿Shader "Custom/Cookbook Shaders/Chapter 3/UnitySpecularVertex" 
{
	Properties 
	{
		_MainTexture ("MainTexture", 2D) = "white" {}
		_MainTint ("MainTint", Color) = (1, 1, 1, 1)
		_SpecularColor ("SpecularColor", Color) = (1, 1, 1, 1)
		_SpecularPower ("SpecularPower", Range(0, 30)) = 1
		
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Phong

		sampler2D _MainTexture;
		float4 _MainTint;	
		float4 _SpecularColor;
		float _SpecularPower;
		
		struct Input 
		{
			float2 uv_MainTexture;
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			half4 c = tex2D (_MainTexture, IN.uv_MainTexture) * _MainTint;
			
			o.Specular = _SpecularPower;
			o.Gloss = 1.0;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		
		inline fixed4 LightingPhong (SurfaceOutput output, fixed3 lightDir, half3 viewDir, fixed atten)
		{
			float diff = dot (output.Normal, lightDir);
			float3 reflectionVector = normalize (2.0 * output.Normal * diff - lightDir);
			
			float spec = pow (max(0, dot (reflectionVector, viewDir)), _SpecularPower);
			float finalSpec = _SpecularColor.rgb * spec;
			
			fixed4 c;
			c.rgb = (output.Albedo * _LightColor0.rgb * diff) + (_LightColor0.rgb * finalSpec);
			c.a = 1.0;
			
			return c;
		}
		
		ENDCG
	} 
	FallBack "Diffuse"
}
