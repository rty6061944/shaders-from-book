﻿Shader "Custom/Cookbook Shaders/Chapter 4/SimpleCubemapWithMask" 
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_ReflMask ("Reflection Mask", 2D) = "white" {}
		_MainTint ("Tint", Color) = (1, 1, 1, 1)
		_Cubemap ("Cubemap", Cube) = "" {}
		_ReflAmount ("Reflection Amount", Range(0, 1)) = 1
	}
	
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D _ReflMask;
		sampler2D _MainTex;
		samplerCUBE _Cubemap;
		float4 _MainTint;
		float _ReflAmount;

		struct Input 
		{
			float2 uv_MainTex;
			float3 worldRefl;
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			float3 reflection = texCUBE(_Cubemap, IN.worldRefl).rgb;
			float4 reflMask = tex2D (_ReflMask, IN.uv_MainTex);
			
			o.Emission = (reflection * reflMask.r) * _ReflAmount;
			o.Albedo = c.rgb * _MainTint;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
