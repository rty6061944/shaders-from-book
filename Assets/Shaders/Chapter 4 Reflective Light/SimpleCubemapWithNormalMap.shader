﻿Shader "Custom/Cookbook Shaders/Chapter 4/SimpleCubemapWithNormalMap" 
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Normal_Map ("Normal Map", 2D) = "white" {}
		_MainTint ("Tint", Color) = (1, 1, 1, 1)
		_Cubemap ("Cubemap", Cube) = "" {}
		_ReflAmount ("Reflection Amount", Range(0, 1)) = 0.5
	}
	
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D _Normal_Map;
		sampler2D _MainTex;
		samplerCUBE _Cubemap;
		float4 _MainTint;
		float _ReflAmount;

		struct Input 
		{
			float2 uv_MainTex;
			float2 uv_Normal_Map;
			float3 worldRefl;
			INTERNAL_DATA
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			float3 normals = UnpackNormal(tex2D(_Normal_Map, IN.uv_Normal_Map)).rgb;
			
			o.Normal = normals;
			
			o.Emission = texCUBE(_Cubemap, WorldReflectionVector (IN, o.Normal)).rgb * _ReflAmount;
			o.Albedo = c.rgb * _MainTint;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
