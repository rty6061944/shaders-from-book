﻿Shader "Custom/Cookbook Shaders/Chapter 5/AutoColor" 
{

	Properties 
	{
		_MainTint ("MainTint", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_SpecularColor ("SpecColor", Color) = (1,1,1,1)
		_SpecPower ("SpecPower", Range(0.01, 30)) = 3
		_ReflCube ("Reflection Cube", CUBE) = "" {}
		_BRDFTex ("BRDF Texture", 2D) = "white" {}
		
		_DiffusePower ("Diffuse Power", Range(0.01, 10)) = 0.5
		_FalloffPower ("Falloff Spread", Range(0.01, 10)) = 3
		_ReflAmmount ("Reflection Ammount", Range(0.01, 10)) = 0.5
		_ReflPower ("Reflection Power", Range(0.01, 3)) = 2.0
	}
	
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf AutoColor 

		sampler2D _MainTex;
		sampler2D _BRDFTex;
		samplerCUBE _ReflCube;
		
		float4 _MainTint;
		float4 _SpecularColor;
		float _SpecPower;
		float _DiffusePower;
		float _FalloffPower;
		float _ReflAmmount;
		float _ReflPower;
		
		inline fixed4 LightingAutoColor (SurfaceOutput s, fixed3 lightDir, half3 viewDir, fixed atten)
		{
			half3 h = normalize(lightDir + viewDir);
			fixed diff = max (0, dot (s.Normal, lightDir));
			
			float ahdn = 1 - dot (h, normalize(s.Normal));
			ahdn = pow (clamp(ahdn, 0.0, 1.0), _DiffusePower);
			
			half4 brdf = tex2D (_BRDFTex, float2(diff, 1 - ahdn));
			
			float nh = max (0, dot(s.Normal, h));
			float spec = pow (nh, s.Specular * _SpecPower) * s.Gloss;
			
			fixed4 c1;
			c1.rgb = (s.Albedo * _LightColor0.rgb * brdf.rgb + _LightColor0.rgb * _SpecularColor.rgb * spec) * (atten * 2);
			c1.a =s.Alpha + _LightColor0.a * _SpecularColor.a * spec * atten;
			
			return c1;
		}
		
		struct Input 
		{
			float2 uv_MainTex;
			float3 worldRefl;
			float3 viewDir;
		};

		void surf (Input IN, inout SurfaceOutput o)
		{
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			
			fixed falloff = saturate (1 - dot(normalize(IN.viewDir), o.Normal));
			falloff = pow (falloff, _FalloffPower);
			
			o.Albedo = c.rgb * _MainTint;
			o.Emission = pow ((texCUBE(_ReflCube, IN.worldRefl).rgb * falloff), _ReflPower) * _ReflAmmount;
			o.Specular = c.r;
			o.Gloss = 1.0;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}