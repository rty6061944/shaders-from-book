﻿Shader "Custom/Cookbook Shaders/Chapter 5/ClothShader" 
{

	Properties 
	{
		_MainTint ("Main Tint", Color) = (1,1,1,1)
		_BumpMap ("Bump Map", 2D) = "bump" {}
		_DetailBumpMap ("Detail Bump Map", 2D) = "bump" {}
		_MainTex ("Main Texture", 2D) = "white" {}
		
		_FresnelColor ("Fresnel Color", Color) = (1,1,1,1)
		_FresnelPower ("Fresnel Power", Range(0, 12)) = 3
		_RimPower ("Rim Power", Range(0, 12)) = 3
		
		_SpecIntencity ("Specular Intencity", Range(0, 1)) = 0.2
		_SpecWidth ("Specular Width", Range(0, 1)) = 0.2
	}
	
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Velvet
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _BumpMap;
		sampler2D _DetailBumpMap;
		
		float4 _MainTint;
		float4 _FresnelColor;
		
		float _FresnelPower;
		float _RimPower;
		float _SpecIntencity;
		float _SpecWidth;
		
		struct Input
		{
			float2 uv_MainTex;
			float2 uv_BumpMap;
			float2 uv_DetailBumpMap;
		};
		
		inline fixed4 LightingVelvet (SurfaceOutput s, fixed3 lightDir, half3 viewDir, fixed atten)
		{
			lightDir = normalize(lightDir);
			viewDir = normalize(viewDir);
			float3 halfVec = normalize(lightDir + viewDir);
			float NdotL = max(0, dot (s.Normal, lightDir));

			float NdotH = max(0, dot(s.Normal, halfVec));
			float spec = pow(NdotH, s.Specular * 128.0) * s.Gloss;
			
			float NdotV = pow(1 - max(0, dot(halfVec, viewDir)), _FresnelPower);
			float NdotE = pow(1 - max(0, dot(s.Normal, viewDir)), _RimPower);
			float finalSpecMask = NdotE * NdotV;
			
			fixed4 c;
			c.rgb = (s.Albedo * NdotL * _LightColor0.rgb) + (spec * (finalSpecMask * _FresnelColor)) * (atten * 2);
			c.a = 1.0;
			
			return c;
		}
		
		void surf (Input IN, inout SurfaceOutput o)
		{
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			fixed3 normals = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap)).rgb;
			fixed3 detailNormals = UnpackNormal( tex2D(_DetailBumpMap, IN.uv_DetailBumpMap)).rgb;
			fixed3 finalNormals = float3(normals.x + detailNormals.x, normals.y + detailNormals.y, normals.z + detailNormals.z);
			
			o.Normal = normalize(finalNormals);
			o.Albedo = c.rgb * _MainTint;
			o.Specular = _SpecWidth;
			o.Gloss = _SpecIntencity;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}