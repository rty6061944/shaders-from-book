﻿Shader "Custom/Cookbook Shaders/Chapter 5/SkinShader" 
{

	Properties 
	{
		_MainTint ("Main Tint", Color) = (1,1,1,1)
		_MainTex ("Main Texture", 2D) = "white" {}
		_BumpMap ("Bump Map", 2D) = "bump" {}
		_CurveScale ("Curvature Scale", Range(0.001, 0.09)) = 0.01
		_CurveAmount ("Curvature Amount", Range(0, 1)) = 0.5
		_BumpBias ("Bumped Bias", Range(0, 5)) = 2.0
		_BRDFTex ("BRDF Texture", 2D) = "white" {}
		_RimPower ("Rim Power", Range(0, 5)) = 2
		_RimColor ("Rim Color", Color) = (1,1,1,1)
		_SpecIntencity ("Specular Intencity", Range(0, 1)) = 0.4
		_SpecWidth ("Specular Width", Range(0, 1)) = 0.2
		_FresnelValue ("Fresnel Value", Range(0.01, 0.3)) = 0.05
	}
	
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf SkinShader
		#pragma target 3.0
	//	#pragma only_renderers d3d9 

		sampler2D _MainTex;
		sampler2D _BRDFTex;
		sampler2D _BumpMap;
		
		float4 _MainTint;
		float4 _RimColor;
		
		float _CurveAmount;
		float _CurveScale;
		float _BumpBias;
		float _FresnelValue;
		float _SpecIntencity;
		float _SpecWidth;
		float _RimPower;
		
		struct SurfaceOutputSkin 
		{
			fixed3 Albedo;
			fixed3 Normal;
			fixed3 Emission;
			fixed3 Specular;
			fixed3 BlurredNormals;
			
			fixed Gloss;
			fixed Alpha;
			fixed Curvature;
		};
		
		struct Input
		{
			float2 uv_MainTex;
			float3 worldPos;
			float3 worldNormal;
			
			INTERNAL_DATA
		};
		
		inline fixed4 LightingSkinShader (SurfaceOutputSkin s, fixed3 lightDir, half3 viewDir, fixed atten)
		{
			lightDir = normalize(lightDir);
			viewDir = normalize(viewDir);
			s.Normal = normalize(s.Normal);
			
			float NdotL = dot (s.BlurredNormals, lightDir);
			float3 halfVec = normalize(lightDir + viewDir);
			
			half3 brdf = tex2D (_BRDFTex, float2((NdotL * 0.5 + 0.5) * atten, s.Curvature)).rgb;
			
			float fresnel = saturate(pow(1 - dot(viewDir, halfVec), 5.0));
			fresnel += _FresnelValue * (1 - fresnel);
			float rim = saturate(pow(1 - dot(viewDir, s.BlurredNormals), _RimPower)) * fresnel;
		
			
			float specBase = max(0, dot(s.Normal, halfVec));
			float spec = pow(specBase, s.Specular * 128.0) * s.Gloss;
		
			fixed4 c;
			c.rgb = (s.Albedo * brdf * _LightColor0.rgb * atten) + (spec + (rim * _RimColor));
			c.a = 1.0;
			
			return c;
		}
		
		void surf (Input IN, inout SurfaceOutputSkin o)
		{
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			fixed3 normals = UnpackNormal(tex2D(_BumpMap, IN.uv_MainTex));
			fixed3 normalBlur = UnpackNormal(tex2Dbias (_BumpMap, float4(IN.uv_MainTex, 0.0, _BumpBias)));
			float curvature = length(fwidth(WorldNormalVector(IN, normalBlur))) / length(fwidth(IN.worldPos)) * _CurveScale;
			
			o.Normal = normals;
			o.BlurredNormals = normalBlur;
			o.Albedo = c.rgb * _MainTint;
			o.Curvature = curvature;
			o.Specular = _SpecWidth;
			o.Gloss = _SpecIntencity;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}