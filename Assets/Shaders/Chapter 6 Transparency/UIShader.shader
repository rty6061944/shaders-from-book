﻿Shader "UI/Default_OverlayNoZTest1"
 {
     Properties
     {
         _MaskTex ("Mask Texture", 2D) = "white" {}
         _Color ("Tint", Color) = (1,1,1,1)
         _Stencil ("Stencil ID", Float) = 0
     }
 
     SubShader
     {
         Stencil
         {
             Ref [_Stencil]
         }
 
         Cull Off
         Lighting Off
         ZWrite Off
         ZTest Off
         Blend SrcAlpha OneMinusSrcAlpha

         Pass
         {
         CGPROGRAM
             #pragma vertex vert noforwardadd
             #pragma fragment frag
             #include "UnityCG.cginc"
             
             struct appdata_t
             {
                 fixed4 vertex   : POSITION;
                 fixed4 color    : COLOR;
                 fixed2 texcoord : TEXCOORD0;
             };
 
             struct v2f
             {
                 fixed4 vertex   : SV_POSITION;
                 fixed4 color    : COLOR;
                 fixed2 texcoord : TEXCOORD0;
             };
             
             fixed4 _Color;
 
             v2f vert(appdata_t IN)
             {
                 v2f OUT;
                 OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
                 OUT.texcoord = IN.texcoord;
                 OUT.color = IN.color * _Color;
                 
                 return OUT;
             }
 
             sampler2D _MaskTex;
 
             fixed4 frag(v2f IN) : SV_Target
             {
                 fixed4 color = IN.color;
                 fixed4 maskColor = tex2D(_MaskTex, IN.texcoord);
                 
                 color.a = 1 - maskColor.a;             
                 return color;
             }
         ENDCG
         }
     }
 }
