﻿Shader "Custom/Cookbook Shaders/Chapter 6/UIWindow" 
{
	Properties 
	{
		_SourceText ("_SourceText", 2D) = "white" {}
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_TransMap ("Transparency value", Range (0,1)) = 0.5
	}
	
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert alpha

		sampler2D _SourceText;
		sampler2D _MainTex;
		float _TransMap;

		struct Input 
		{
			float2 uv_MainTex;
			float2 uv_SourceText;
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			half4 source = tex2D (_SourceText, IN.uv_SourceText);
			
			if(c.a > 0.5)
			{
				o.Alpha = _TransMap;
			}
			else
			{
				o.Alpha = 1;
			}
			
			o.Albedo = source.rgb;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
