﻿Shader "Custom/Cookbook Shaders/Chapter 6/ZWrite OverLay" 
{
	Properties 
	{
		_GUITex ("GUI Texture", 2D) = "white" {}
		_Color ("Color", Color) = (1, 1, 1, 1)
		_FadeValue ("Fade Value", Range(0, 1)) = 1
	}
	
	SubShader 
	{
		Tags { "RenderType"="Opaque" "Queue"="Overlay" "IgnoreProjector"="True" }
			 
		ZWrite Off
		ZTest Always
		Cull Off
		
		LOD 200
		
		CGPROGRAM
		#pragma surface surf UnlitGUI alpha novertexlights

		sampler2D _GUITex;
		float4 _Color;
		float _FadeValue;

		struct Input 
		{
			float2 uv_GUITex;
		};

		inline fixed4 LightingUnlitGUI (SurfaceOutput s, fixed3 lightDir, fixed3 viewDir, fixed atten)
		{
			fixed4 c;
			c.rgb = s.Albedo;
			c.a = s.Alpha;
			
			return c;
		}

		void surf (Input IN, inout SurfaceOutput o) 
		{
			half4 c = tex2D (_GUITex, IN.uv_GUITex);
			o.Albedo = c.rgb * _Color.rgb;
			o.Alpha = c.a * _FadeValue;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}