﻿Shader "Custom/Cookbook Shaders/Chapter 7/VertexForTerrain" 
{
	Properties 
	{
		_MainTex ("Main texture", 2D) = "white" {}
		_SecTex ("Secondary texture", 2D) = "white" {}
		_HeightMap ("Height map", 2D) = "white" {}
		
		_Value ("Value", Range(0, 20)) = 3
	}
	
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		Pass 
		{
			ColorMaterial AmbientAndDiffuse
		}
		
		CGPROGRAM
		#pragma surface surf Lambert vertex:vert
		#pragma target 3.0
		
		sampler2D _MainTex;
		sampler2D _SecTex;
		sampler2D _HeightMap;
		float _Value;
		
		struct Input 
		{
			float2 uv_MainTex;
			float2 uv_SecTex;
			float2 vertexColor;
		};

		void vert (inout appdata_full v, out Input o)
		{
			o.vertexColor = v.color.rgb;
		}

		void surf (Input IN, inout SurfaceOutput o) 
		{
			fixed4 main = tex2D (_MainTex, IN.uv_MainTex);
			fixed4 secondary = tex2D (_SecTex, IN.uv_SecTex);
			fixed4 height = tex2D (_HeightMap, IN.uv_MainTex);
			
			float redChannel = 1- IN.vertexColor.r;
			float rHeight = height.r * redChannel;
			float invertHeight = 1 - height.r;
			float finalHeight = (invertHeight * redChannel) * 4;
			float finalblend = saturate(rHeight + finalHeight);
			
			float hardness = ((1 - IN.vertexColor.g) * _Value) + 1;
			finalblend = pow (finalblend, hardness);
			
			float3 finalColor = lerp (main, secondary, finalblend);
			
			o.Albedo = finalColor;
			o.Alpha = main.a;
		}
		ENDCG
	}

	FallBack "Diffuse"
}
