﻿Shader "Custom/Cookbook Shaders/Chapter 7/VertexPositions" 
{
	Properties 
	{
		_ColorA ("Color A", Color) = (1,1,1,1)
		_ColorB ("Color B", Color) = (1,1,1,1)
		_MainTex ("Main Texture", 2D) = "white" {}
		_MainTint ("Main Tint", Range(0, 1)) = 0.5
		
		_Speed ("Speed", Range(0.1, 80)) = 10
		_Frequency ("Frequency", Range(0, 5)) = 2
		_Amplitude ("Amplitude", Range(-1, 1)) = 0.5
	}
	
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert vertex:vert
		#pragma target 3.0

		sampler2D _MainTex;
		float4 _ColorA;
		float4 _ColorB;
		
		float _MainTint;
		float _Speed;
		float _Frequency;
		float _Amplitude;
		float _OffsetVal;

		struct Input 
		{
			float2 uv_MainTex;
			float3 vertColor;
		};

		void vert (inout appdata_full v, out Input o)
		{
			float time = _Time.x * _Speed;
			float waveValueA = sin(time + v.vertex.x * _Frequency) * _Amplitude;
			
			v.vertex.xyz = float3 (v.vertex.x, v.vertex.y + waveValueA, v.vertex.z);
		//	v.normal = normalize (float3 (v.vertex.x + waveValueA, v.vertex.y, v.vertex.z));
			o.vertColor = float3(waveValueA, waveValueA, waveValueA);
		}

		void surf (Input IN, inout SurfaceOutput o) 
		{
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			float3 tintColor = lerp(_ColorA, _ColorB, IN.vertColor).rgb;
			
			o.Albedo = c.rgb * tintColor * _MainTint;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
