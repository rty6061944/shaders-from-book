﻿Shader "Custom/Cookbook Shaders/Chapter 8/CustomDiffuse" 
{
	Properties 
	{
		_MainTex ("Main Tex", 2D) = "white" {}
		_NormalMap ("Normal Map", 2D) = "white" {}
	}
	
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf SimpleLambert
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _NormalMap;

		struct Input 
		{
			float2 uv_MainTex;
			float2 uv_NormalMap;
		};

		inline float4 LightingSimpleLambert (SurfaceOutput o, float3 lightDir, float atten)
		{
			float diff = max (0, dot (o.Normal, lightDir));
			float4 c;
			
			c.rgb = o.Albedo * _LightColor0.rgb * (diff * atten * 2);
			c.a = o.Alpha;
			
			return c;
		}


		void surf (Input IN, inout SurfaceOutput o) 
		{
			float4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
			
			o.Normal = UnpackNormal (tex2D (_NormalMap, IN.uv_NormalMap));
		}
		ENDCG
	} 
	FallBack "Diffuse"
}