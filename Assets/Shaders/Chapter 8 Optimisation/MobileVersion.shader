﻿Shader "Custom/Cookbook Shaders/Chapter 8/MobileVersion" 
{
	Properties 
	{
		_MainTex ("Main Tex", 2D) = "white" {}
		_NormalMap ("Normal Map", 2D) = "white" {}
		_SpecIntencity ("Specular Width", Range(0.01, 1)) = 0.5
	}
	
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf MobileBlinnPhong exclude_path:prepass nolightmap noforwardadd halfasview
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _NormalMap;
		fixed _SpecIntencity;

		struct Input 
		{
			half2 uv_MainTex;
		};

		inline fixed4 LightingMobileBlinnPhong (SurfaceOutput o, fixed3 lightDir, fixed3 halfVec,fixed atten)
		{
			fixed diff = max (0, dot (o.Normal, lightDir));
			fixed nh = max (0, dot (o.Normal, halfVec));
			fixed spec = pow (nh, o.Specular * 128) * o.Gloss;
			fixed4 c;
			
			c.rgb = (o.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * spec) * (atten * 2);
			c.a = 0.0;
			
			return c;
		}

		void surf (Input IN, inout SurfaceOutput o) 
		{
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			o.Alpha = 0.0;
			o.Gloss = c.a;
			o.Specular = _SpecIntencity;
			
			o.Normal = UnpackNormal (tex2D (_NormalMap, IN.uv_MainTex));
		}
		ENDCG
	} 
	FallBack "Diffuse"
}