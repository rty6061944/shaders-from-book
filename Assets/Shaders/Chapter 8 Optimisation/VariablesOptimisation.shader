﻿Shader "Custom/Cookbook Shaders/Chapter 8/VariablesOptimisation" 
{
	// changed float to half and fixed
	// added #pragma noforwardadd
	// use only one UV coordinates IN.uv_MainTex
	// removed IN.uv_NormalMap from Input
	// added exclude_path:prepass to pragma

	Properties 
	{
		_MainTex ("Main Tex", 2D) = "white" {}
		_NormalMap ("Normal Map", 2D) = "white" {}
	}
	
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf SimpleLambert exclude_path:prepass noforwardadd
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _NormalMap;

		struct Input 
		{
			half2 uv_MainTex;
		};

		inline fixed4 LightingSimpleLambert (SurfaceOutput o, fixed3 lightDir, fixed atten)
		{
			fixed diff = max (0, dot (o.Normal, lightDir));
			fixed4 c;
			
			c.rgb = o.Albedo * _LightColor0.rgb * (diff * atten * 2);
			c.a = o.Alpha;
			
			return c;
		}

		void surf (Input IN, inout SurfaceOutput o) 
		{
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb;
			o.Alpha = c.a;
			
			o.Normal = UnpackNormal (tex2D (_NormalMap, IN.uv_MainTex));
		}
		ENDCG
	} 
	FallBack "Diffuse"
}