﻿Shader "Custom/Cookbook Shaders/Chapter 9/FirstExample" 
{
	Properties 
	{
		_MainTex ("Main Texture", 2D) = "white" {}
		_Desaturate ("Desaturate", Range(0, 1)) = 0.5
		_MyColor ("My Color from CGInclude", Color) = (1, 1, 1, 1)
	}
	
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#define HalfLambert
		
		#include "NewCGInclude.cginc"
		#pragma surface surf CustomLambert
		#pragma target 3.0

		sampler2D _MainTex;
		float _Desaturate;

		struct Input 
		{
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			c.rgb = lerp (c.rgb, Luminance(c.rgb), _Desaturate);
			
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
